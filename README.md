# Talking SMAAC

Arduino source code used as part of [Talking SMAAC: A New Tool to Measure Soil Respiration and Microbial Activity](https://www.frontiersin.org/articles/10.3389/feart.2019.00138/full)

# Required Arduino libraries
* [SD](https://www.arduino.cc/en/Reference/SD)
* [Wire](https://www.arduino.cc/en/Reference/Wire) - for interfacing with the RTC and the CO2 sensor
* [SPI](https://www.arduino.cc/en/Reference/SPI) - for interfacing with the datalogger shield (Adafruit, [Product page](https://www.adafruit.com/product/1141), [Documentation](https://learn.adafruit.com/adafruit-data-logger-shield))
* [NDIR_I2C](https://github.com/SandboxElectronics/NDIR) - for controlling/interfacing with the CO2 Sensor
* [RTClib](https://github.com/adafruit/RTClib) - needed for the Real Time Clock (RTC)


# Hardware
* [Arduino Uno](https://store.arduino.cc/usa/arduino-uno-rev3)
* Adafruit [Data Logger Shield](https://www.adafruit.com/product/1141)
* Sandbox Electronics [10,000ppm MH-Z16 NDIR CO2 Sensor with I2C/UART 5V/3.3V Interface for Arduino/Raspeberry Pi](https://sandboxelectronics.com/?product=mh-z16-ndir-co2-sensor-with-i2cuart-5v3-3v-interface-for-arduinoraspeberry-pi)
* Power supply

# Configuration

* Connect the wires as shown in [Figure 1](https://www.frontiersin.org/files/Articles/457635/feart-07-00138-HTML/image_m/feart-07-00138-g001.jpg)
	* Red -> +5 V
	* Black -> Ground
	* Green -> SCL
	* Yellow -> SDA
* Connect the power supply or USB cable to the Arduino
* Upload the program using the Arduino IDE
* Set the RTC time
	A sync message is sent from the Arduino serial interface in the form of "Txxxxxxxxxx" where the x's are replaced with the current unix timestamp.
	EX: T1548957540 would set the time to 01/31/2019 @ 5:59pm (UTC)
