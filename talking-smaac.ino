#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <NDIR_I2C.h>
#include "RTClib.h"
#include "rtc_sync.h"

NDIR_I2C mySensor(0x4D); //Adaptor's I2C address (7-bit, default: 0x4D)
const int chipSelect = 10;

const int sensorEnable = 8;               // control indicator LED to show that the sensor is taking a reading
const int datePrint = 2;                  // pin 2 is used for debugging. If pin 2 is connected to ground, the date will not be printed to the serial console allowing the arduino serial plotter to function correctly
unsigned long previousMillis = 0;         // see https://www.arduino.cc/en/Tutorial/BlinkWithoutDelay for more


// constants won't change :
const long read_interval = 1000;          // 1000 miliseconds (1 second) is the delay betweeen readings
const long setup_interval = 10000;        // 10,000 miliseconds (10 seconds) is the sensor warm up/stabilization period



File logfile;
char filename[] = "CO2_LOG.csv";          // filename for data file on SD card

// converts DateTime object and returns a string with propper formatting
//      EX: "2019-01-31 17:05:01"
String prettyDate(DateTime dt)
{
    String dataString = "";
    dataString += String(dt.year());
    dataString += "-";
    dataString += String(dt.month());
    dataString += "-";
    dataString += String(dt.day());
    dataString += " ";
    if (dt.hour()<10)
    {
      dataString += "0";
    }
    dataString += String(dt.hour());
    dataString += ":";
    if (dt.minute()<10)
    {
      dataString += "0";
    }
    dataString += String(dt.minute());
    dataString += ":";
    if (dt.second()<10)
    {
      dataString += "0";
    }
    dataString += String(dt.second());
    return dataString;
}

void setup()
{
  // enable serial console
  Serial.begin(9600);

  // set up I/O pins
  pinMode(sensorEnable, OUTPUT);
  pinMode(datePrint, INPUT_PULLUP);

  // 
  Serial.print("Initializing SD card...");
  // see if the card is present and can be initialized:
  if (!SD.begin(chipSelect))
  {
    Serial.println("Card failed, or not present");
    while (1)
    {
      digitalWrite(sensorEnable, HIGH);
      delay(1000);
      digitalWrite(sensorEnable, LOW);
      delay(1000);
      digitalWrite(sensorEnable, HIGH);
      delay(500);
      digitalWrite(sensorEnable, LOW);
      delay(500);
    }
  }
  Serial.println("card initialized.");
  
  // if the file is available, write to it:
  if (logfile)
  {
    Serial.println("File OK!");
    logfile.close();
  }
  
  // initialize the RTC chip
  // if the RTC cannot be initialized, halt
  if (! rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  
  if (! rtc.initialized()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
     rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }

  // initialize the sensor
  if (mySensor.begin())
  {
    // disable autocalibration, see readme file for more information
    Serial.println("Disabling Autocalibration");
    mySensor.disableAutoCalibration();
    
    Serial.println("Wait 10 seconds for sensor initialization...");
    
    previousMillis = millis();

    // wait 10 seconds for the sensor to warm up
    while (true)
    {
      unsigned long currentMillis = millis();
      if (currentMillis - previousMillis >= setup_interval)
      {
        break;
      }

      // flash the led rapidly during setup
      while (true)
      {
        unsigned long currentMillis = millis();
        unsigned long ledpreviousMillis = 0;
        unsigned long led_interval = 100;       // LED interval time is 100 miliseconds
        
        if (currentMillis - ledpreviousMillis >= led_interval)
        {
          bool LED_STATE = digitalRead(sensorEnable);
          digitalWrite(sensorEnable, !LED_STATE);
          break;
        }
        
        
        // during the setup period, process any rtc syncronization messages
        syncRTC();
      }
    }
  }
  else            // halt if the sensor isn't communicating. 
  {
    Serial.println("ERROR: Failed to connect to the sensor.");
    while(1);
  }
}


void loop()
{
  // turn on the indicator LED
  digitalWrite(sensorEnable, HIGH);
  previousMillis = millis();
  while (true)
  {
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= read_interval)
    {
      break;
    }
    syncRTC();
  }
 
  // prepare the message for logging
  // format is <DATE>,<CO2 Concentration>
  
  String disp = "";

  // get the current timestamp
  DateTime now = rtc.now();
        
  disp += prettyDate(now);
  disp += ",";
  if (mySensor.measure()) 
  {
    disp += mySensor.ppm;
  }
  else 
  {
    Serial.println("Sensor communication error.");
  }

  // turn off the indicator LED
  digitalWrite(sensorEnable, LOW);
  
  // check the debug pin to see if the date/time information should be displayed
  // the pin has the internal pullup resistor enabled, so unless the pin is grounded, the full date + concentration will be printed to the serial console
  if (digitalRead(datePrint) == LOW)
  {
    Serial.println(disp);
  }
  else
  {
    // only print the concentration
    Serial.println(mySensor.ppm);
  }

  // the full date + concentration will always be logged even if the debug pin is grounded
  logfile = SD.open(filename, FILE_WRITE);
  logfile.println(disp);
  logfile.close();
  
  delay(10);

  // wait for the next sensor read time
  previousMillis = millis();
  while (true)
  {
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= read_interval)
    {
      break;
    }
    syncRTC();
  }
}
